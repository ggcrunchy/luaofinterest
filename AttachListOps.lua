--- Utilities to build mixin operations for objects that can be attached, along with other
-- objects, to a parent, where order may be important.
--
-- The parent and neighbor object state is unique to each build, and thus an object type can
-- make several sets of operations if it has more complex membership needs.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--
-- Standard library imports --
local assert = assert

-- Imports --
local IsType = class.IsType
local New = class.New
local NoOp = func_ops.NoOp
local Weak = table_ops.Weak

--- Builds a set of operations that can be set directly, or composed into, member functions
-- of attachable hierarchical objects.
-- @param base Class name of objects' base class.
-- @treturn table Operations table with the following methods:
--
-- * **attach**: Called as
--    parent:attach(object, on_attach, ...),
-- where any varargs will be passed to _on\_attach_.
--
-- If _object_ is not already attached to _parent_, it is first detached from its current
-- parent, if it has one, and added to the end of _parent_'s attach list.
--
-- If provided, the _on_attach_ logic is then called as
--    on_attach(parent, object, is_new_parent, ...),
-- where _is\_new\_parent_ is true if _object_ was not already attached to _parent_.
--
-- * **chain_iter**: Iterator, called as
--    object:chain_iter(),
-- will traverse up the hierarchy, returning each object along the way, starting with _object_
-- itself.
--
-- * **detach**: Called as
--    object:detach(on_detach, ...),
-- where any varargs will be passed to _on\_detach_.
--
-- If _object_ has no parent, this is a no-op.
--
-- If provided, the _on\_detach_ logic is first called as
--    on_detach(object, parent, ...).
--
-- Following this, _object_ is removed from its parent's attach list.
--
-- * **get_back**: Called as
--    parent:get_back(),
-- will return the last object in _parent_'s attach list, or **nil** if it is empty.
--
-- * **get_head**: Called as
--    parent:get_head(),
-- will return the first object in _parent_'s attach list, or **nil** if it is empty.
--
-- * **get_parent**: Called as
--    object:get_parent(),
-- will return _object_'s parent, or **nil** if _object_ is unattached.
--
-- * **is_attached**: Predicate, called as
--    object:is_attached().
--
-- * **list_iter**: Iterator, called as
--    parent:list_iter(reverse),
-- will iterate over _parent_'s attach list, returning each object along the way.
--
-- If _reverse_ is true, it will traverse back-to-front, otherwise front-to-back.
--
-- * **promote**: Called as
--    object:promote(),
-- will move _object_ to the front of its parent's attach list, if attached.
--
-- @return Unique member key for an object's attach list, of type @{OrderedSet}. An object
-- will not be given a list until necessary.
-- @treturn table Parents weak table, keyed by child objects.
-- @see class.Define
function Build (base)
	-- Unique member keys --
	local _attach_list = {}

	-- List operations --
	local Ops = {}

	-- Parent list --
	local Parents = Weak("k")

	-- Attach op --
	function Ops:attach (object, on_attach, ...)
		assert(IsType(object, base), "Cannot attach object: wrong base type")
		assert(self ~= object, "Cannot attach object to self")

		-- If the object is being assigned a new parent, attach it.
		local is_new_parent = Parents[object] ~= self

		if is_new_parent then
			object:Detach()

			-- Lazily build a list if necessary.
			self[_attach_list] = self[_attach_list] or New("OrderedSet")

			-- Attach the object and link its parent.
			self[_attach_list]:PutInBack(object)

			Parents[object] = self
		end

		-- Apply attach logic.
		(on_attach or NoOp)(self, object, is_new_parent, ...)
	end

	do
		-- Chain iteration body
		local function Iter (O, object)
			if object then
				return Parents[object]
			end

			return O
		end

		-- Chain iteration op --
		function Ops:chain_iter ()
			return Iter, self
		end
	end

	-- Detach op --
	function Ops:detach (on_detach, ...)
		local parent = Parents[self]

		if parent then
			(on_detach or NoOp)(self, parent, ...)

			parent[_attach_list]:Remove(self)

			Parents[self] = nil
		end
	end

	-- Back element op --
	function Ops:get_back ()
		local attach_list = self[_attach_list]

		if attach_list then
			return attach_list:Back()
		end
	end

	-- Head element op --
	function Ops:get_head ()
		local attach_list = self[_attach_list]

		if attach_list then
			return attach_list:Front()
		end
	end

	-- Parent op --
	function Ops:get_parent ()
		return Parents[self]
	end

	-- Attach predicate op --
	function Ops:is_attached ()
		return Parents[self] ~= nil
	end

	-- List iteration op --
	function Ops:list_iter (reverse)
		local attach_list = self[_attach_list]

		if not attach_list then
			return NoOp
		elseif reverse then
			return attach_list:BackToFrontIter()
		else
			return attach_list:FrontToBackIter()
		end
	end

	-- Promote op --
	function Ops:promote ()
		local parent = Parents[self]

		if parent then 
			parent[_attach_list]:PutInFront(self)
		end
	end

	return Ops, _attach_list, Parents
end