-----------
-- Imports
-----------
local assert, type = assert, type
local acos, max, min, random = math.acos, math.max, math.min, math.random
local CosSin = math_ex.CosSin
local GetObject = objects.GetObject
local GetPositionOfCollisionInTrace = engine.GetPositionOfCollisionInTrace
local GetTimeDifference = engine.GetTimeDifference
local LookAt = math_ex.LookAt
local New = class.New
local NewEntity = objects.NewEntity
local Rand = math_ex.Rand
local RotateIndex = numericops.RotateIndex
local Type = class.Type


-----------------------
-- Intermediate matrix
-----------------------
local Matrix = New("Matrix")

------------------------------------------------
-- Current orientation; intermediate quaternion
------------------------------------------------
local CurQuat, Quat = New("Quaternion"), New("Quaternion")

--------------------------------------------------------------
-- Current position; intermediate, target position; up vector
--------------------------------------------------------------
local CurPos, Pos, Target, Up, PosBackup, TempVect = New("Vec3D"), New("Vec3D"), New("Vec3D"), New("Vec3D"), New("Vec3D"), New("Vec3D")
local useCameraInterpolation, framesWithLowFramerate

-- debug
local timesCalled, timeSpent
local lowestTime, biggestTime

-- magic numbers
local zoominduration, zoomoutduration = .2, .2 --.2,.2
local zoominlag, zoomoutlag = .4, .6
local maxFramesWithLowFramerate, framesWithLowFramerateThreashold = 50, 25
local framesToInterpolate, previousTimes


-- Supplies current view
-- V: View handle
-- pos, target: Position and target to assign
----------------------------------------------
local function Current (V, pos, target)
	pos:Set(V.cur.pos)
	target:Set(V.cur.target)
end

-- Updates camera position and orientation
-- V: View handle
-- t: Relative camera time
-------------------------------------------
local function UpdateCamera (V, t)
	(V.blend or Current)(V, Pos, Target, t)

	local ref = V.ref

	ref:SetLocalPos(Pos)
	ref:SetLocalOrientation(LookAt(Pos, Target, Matrix):ToEuler())
end

-- Synchronizes the camera dummies
-- V: View handle
-----------------------------------
local function SyncDummies (V)
	UpdateCamera(V, 0)

	local dummy, ref = V.dummy, V.ref

	dummy:SetRotationMatrix(ref:GetRotationMatrix(Matrix))
	dummy:SetPos(ref:GetPos(Pos))
end

-------------------------
-- View class definition
-------------------------
class.Define("View", {
	-- Adds a view item
	-- name: Item name, or nil to add to the sequence
	-- pos, target: View position, target
	--------------------------------------------------
	Add = function(V, name, pos, target)
		assert(type(name) ~= "number", "Numeric names forbidden")

		local view = { pos = pos:Dup(), target = target:Dup() }

		V.list[name ~= nil and name or #V.list + 1] = view

		if not V.cur then
			V.cur = view

			SyncDummies(V)
		end
		
		messagef("added")
		useCameraInterpolation = true
		framesWithLowFramerate = 0
		timeSpent = 0
		timesCalled = 0
		biggestTime = 0
		lowestTime = 99999
		framesToInterpolate, previousTimes = 4, {0, 0, 0, 0}
		 CurPos, Pos, Target, Up, PosBackup, TempVect = New("Vec3D"), New("Vec3D"), New("Vec3D"), New("Vec3D"), New("Vec3D"), New("Vec3D")
		 Matrix = New("Matrix")
		 CurQuat, Quat = New("Quaternion"), New("Quaternion")
	end,

	-- Returns: True, to persist as a task
	---------------------------------------
	__call = function(V)
		local lapse, dummy, ref = GetTimeDifference(), V.dummy, V.ref
		local tickCount = engine.GetTickCount()
		
		--assert(dummy == GetObject(V.camera:GetParent(), "VisBaseEntity_cl"), "View has lost camera")

		-- Apply shaking.
		local shake = V.shake

		dummy:GetPos(CurPos)

		if shake > 0 then
			Pos:Set(Rand(-5, 5), Rand(-5, 5), Rand(-5, 5))

			CurPos:AddScaled(Pos, V.shakefactor * shake )

			V.shake = max(shake - (2*lapse), 0)
		end

		-- Update toggle, if active.
		V.interp(lapse)

		CurQuat:Set(dummy:GetRotationMatrix(Matrix))
		Quat:Set(ref:GetRotationMatrix(Matrix))
		
		--if the framerate is less than 30 for X amount of times, rise flag to disable camera interpolations
		if useCameraInterpolation and 1 / lapse <= framesWithLowFramerateThreashold then
			framesWithLowFramerate = framesWithLowFramerate + 1
			--printf("Low Framerate count is %d", framesWithLowFramerate)
			if framesWithLowFramerate >= maxFramesWithLowFramerate then
				--printf("Disabling camera interpolation")
				useCameraInterpolation = false
			end
		end
		
		-- Use lag
		if useCameraInterpolation then 
			local prevQuat = CurQuat
			--local fakelapse = 1 / 30
			-- shift previous times and update current one
			for i = #previousTimes, 2, -1 do
				previousTimes[i] = previousTimes[i - 1]
			end
			previousTimes[1] = lapse
			-- determine de average lapse
			local fakelapse = 0
			for i = 1, #previousTimes do
				fakelapse = fakelapse + previousTimes[i]
			end
			fakelapse = fakelapse / framesToInterpolate
			-- Interpolate
			CurQuat:SLerp(CurQuat, Quat, fakelapse * V.camrot_lag) --or 0 * (V.camrot_lag or 0)
			CurPos:SetLerp(CurPos, ref:GetPos(Target), fakelapse * V.campos_lag)
		-- Don't use lag--	
		else
			CurQuat = Quat
			CurPos:SetLerp(CurPos, ref:GetPos(Target), 1)
		end
		
		-- Apply position and rotation
		dummy:SetRotationMatrix(CurQuat:Get(Matrix))
		dummy:SetPos(CurPos)

		-- Test to prevent cam from going below world
		if CurPos.z < 50 then
			CurPos.z = 50			
		end

		
		
		-- try checking if camera is not blocked
		if game.IsRunning() then
			local parententity = GetObject(ref:GetParent())
			if parententity:IsAlive() then
				-- restore view when respawned
				if V.revertview then
					V.revertview = false
					V:Toggle(V.previousview, useCameraInterpolation and zoomoutduration or .5)
				end
				if not V.camadjust then
					local collided = GetPositionOfCollisionInTrace(dummy:GetPos(CurPos), parententity:GetPos(Target))
					--printf("CurPos: x=%0.2f, y=%0.2f, z=%0.2f, TargetPos x=%0.2f, y=%0.2f, z=%0.2f", CurPos.x, CurPos.y, CurPos.z, Target.x, Target.y, Target.z )
					if collided and Type(parententity) == "Player" and not parententity:IsRespawning() then
						-- save colliding view index and force toggle 
						V.collidingViewIndex = V:GetIndex()
						
						--printf("Tried to toggle to autozoom cam")
						V:Toggle("autozoom", useCameraInterpolation and zoominduration or .5)
						V.timetoggling = zoomoutlag
						V.camadjust = true
						V.camrestoring = false
					end
				else
					V.timetoggling = V.timetoggling - lapse
					-- check if cam isn't restoring, it's time to restore and view's not colliding
					if (not V.camrestoring) and (V.timetoggling < 0) then
						TempVect:Set(V.list[V.collidingViewIndex].pos)
						TempVect:Map(parententity:GetRotationMatrix(Matrix))
						TempVect:Add(parententity:GetPos(CurPos))
						-- toggle view back if 'safe'
						local collided = GetPositionOfCollisionInTrace(TempVect, parententity:GetPos(Target))
						if not collided and Type(parententity) == "Player" and parententity:IsAlive() then
							V:Toggle(V.collidingViewIndex, useCameraInterpolation and zoomoutduration or .5)
							V.timetoggling = zoominlag
							V.camrestoring = true
						end
					else
						if V.camrestoring and V.timetoggling < 0 then
							V.camadjust = false
							V.camrestoring = false
						end
					end
				end
			else
				-- if camera isn't zoomed-in due to death, toggle it
				if not V.revertview then
					local camid = random(1, 5)
					V.revertview = true
					V.previousview = V:GetIndex()
					if camid ~= 5 then
						V:Toggle("crashcam" .. camid, .5)
					end
				end
			end
			
			-- debug stuff
			timeSpent = timeSpent + (engine.GetTickCount() - tickCount)
			timesCalled = timesCalled + 1
			local currentAverage = timeSpent / timesCalled
			--printf("Average time on view interpolation: %0.8f s", currentAverage)
			
			if lowestTime > currentAverage then
				lowestTime = currentAverage
			end
			if biggestTime < currentAverage then
				biggestTime = currentAverage
			end
			
		-- debug stuff		
		else
			--printf("Average: %0.8f Lowest: %0.8f Biggest: %0.8f", timeSpent / timesCalled, lowestTime, biggestTime)	
			
		end
		
		

		return true
	end,

	-- Returns: Sequence index
	---------------------------
	GetIndex = function(V)
		--right now we are also using views that have a name rather than an index ( rearcam ) so we check if the index is valid or we use
		--V.curkey for collinding view index
		return V.index ~= 0 and V.index or ( V.curKey or 0 )
	end,
	
	GetCurCamIndex = function (V)
		return V.PrevIndex or 0
	end,

	-- Returns: Interpolator handle
	--------------------------------
	GetInterpolator = function(V)
		return V.interp
	end,

	-- Resets the view sequence
	----------------------------
	ResetSequence = function(V)
		V.index = 0
	end,

	-- poslag, rotlag: Positional, rotational lag rate to assign
	-------------------------------------------------------------
	SetLag = function(V, poslag, rotlag)
		V.campos_lag, V.camrot_lag = poslag, rotlag
	end,

	-- factor: Factor to assign
	--------------------------------
	SetShakeFactor = function(V, factor)
		V.shakefactor = factor
	end,

	-- intensity: Intensity to assign
	-- bAdd: If true, add to current intensity
	-------------------------------------------
	SetShakeIntensity = function(V, intensity, bAdd)
		V.shake = intensity + (bAdd and V.shake or 0)
	end,

	-- target: View target to assign
	-- bNoSync: If true, ignore sync
	---------------------------------
	SetTarget = function(V, target, bNoSync)
		V.ref:AttachToParent(target)

		if not bNoSync then
			SyncDummies(V)
		end
	end,
	
	isMoving =function(V) 
		return V:GetInterpolator():GetMode() == "suspended"
	end,
	
	
	-- Toggles to another view
	-- next: If nil, proceed in the sequence; otherwise, the target view
	-- time: If nil, toggle immediately; otherwise, transition time
	---------------------------------------------------------------------
	
	Toggle = function(V, next, time)
		messagef("toggle")
		assert(time == nil or type(time) == "number" and time >= 0, "Bad time")

		-- Cache the old view. Move to the next view item, either an explicit entry or the
		-- next in the sequence.
		local old, cur = V.cur

		if next ~= nil then
			cur, V.index = assert(V.list[next], "View unavailable"), type(next) == "number" and next or 0
			V.curKey = next

		else
			assert(#V.list > 0, "Empty sequence")

			V.index = RotateIndex(V.index, #V.list)

			cur = V.list[V.index]
		end

		V.cur = cur
		--save the current index as the las numeric index in use
		V.PrevIndex = type (V:GetIndex()) == "number" and V:GetIndex() or V.PrevIndex

		-- Given no time, transition immediately to the next choice.
		if not time then
			V.interp:Stop()

			V.blend = nil

		-- Otherwise, begin toggling if this is not already in progress.
		elseif V.interp:GetMode() == "suspended" then
			-- Cache properties of the old view.
			local pos1, target1 = old.pos, old.target
			local to1 = pos1 - target1
			local radius1 = #to1

			-- Cache properties of the new view.
			local pos2, target2 = cur.pos, cur.target
			local to2 = pos2 - target2
			local radius2 = #to2
			local to2_u = to2 / radius2

			-- If the views are collinear, use a lerp between the positions; otherwise, use a
			-- scaling slerp, offset from the current target. Use a lerp between the targets.
			Up:SetBasePlusScaled(to1, to2_u, -(to1 * to2_u))

			if Up * Up < 1e-3 then
				function V:blend (pos, target, when)
					pos:SetLerp(pos1, pos2, when)
					target:SetLerp(target1, target2, when)
				end

			else
				local up_u, angle = Up / #Up, acos((to1 / radius1) * to2_u)

				function V:blend (pos, target, when)
					local cos, sin = CosSin(angle * (1 - when))

					-- Get the position and target from an interpolated target, angle, and radius.
					target:SetLerp(target1, target2, when)

					pos:SetScaledSum(to2_u, cos, up_u, sin)
					pos:Scale(radius1 + (radius2 - radius1) * when)
					pos:Add(target)
				end
			end

			-- Begin toggling.
			V.interp:SetDuration(time)
			V.interp:Start("once")
		end
	end,
	
	GoToFarCam =function (V, pos,target)
		V.list["farcam"].pos = pos-New("Vec3D", -450, 0, 200)
		V.list["farcam"].target = New("Vec3D", 10, 0, 0)
		--V:Toggle( "farcam", nil ) --nil
		--V.pos=pos
		--V.target=target
	end,
	
	----wrapper to go to the rear camera
	GoToBackCam = function( V , time )
		V:Toggle( "rearcam", time ) --nil
	end,
	
	GoToLeftCam = function( V , time )
		if V:GetIndex() == "reticle" then
			return 
		end
		V.list["leftcam"].pos = ( type(V:GetIndex()) == "number" ) and V.list[ V:GetIndex() ].pos or V.list[ V.PrevIndex ].pos
		V.list["leftcam"].target.y = V.list["leftcam"].pos.x * 1.6 
		V:Toggle( "leftcam", time )
	end,
	
	GoToRightCam = function( V , time )
		if V:GetIndex() == "reticle" then
		return 
		end
		V.list["rightcam"].pos = ( type(V:GetIndex()) == "number" )and V.list[ V:GetIndex() ].pos or V.list[ V.PrevIndex ].pos
		V.list["rightcam"].target.y = V.list["rightcam"].pos.x * -1.6
		V:Toggle( "rightcam", time )
	end,
	
	--wrapper to move to the previous indexed camera ( no named cameras )
	ResetCam = function ( V , time )
		messagef("resetCam")
		V:Toggle( V.PrevIndex , time)
	end
	
},

-- Constructor
-- camera: Camera bound to view
--------------------------------
function(V, camera)
	V.interp, V.list, V.shake, V.shakefactor, V.camera, V.dummy, V.ref = New("Interpolator", function(t)
		UpdateCamera(V, t)
	end), {}, 0, 0, camera, NewEntity(), NewEntity()

	-- Hide the dummies.
	V.dummy:SetVisibleBitmask("invisible")
	V.ref:SetVisibleBitmask("invisible")
	
	--printf("view constructed")
	-- Take possession of the camera.
	--V.dummy:AttachToEntity(pathCamera)
	V.camera:AttachToEntity(V.dummy) --V.dummy

	-- Start at the beginning of the sequence.
	V:ResetSequence()
end)