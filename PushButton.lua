--- Derives from <b><a href="Widget.html">Widget</a></b>.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Imports --
local ButtonStyleRender = widget_ops.ButtonStyleRender
local DrawString = widget_ops.DrawString
local NoOp = func_ops.NoOp
local SuperCons = class.SuperCons

-- Unique member keys --
local _action = {}
local _style = {}
local _sx = {}
local _sy = {}

-- Stock signals --
local Signals = {}

function Signals:drop (group)
	if self == group:GetEntered() then
		(self[_action] or NoOp)(self)
	end
end

function Signals:render (x, y, w, h, group)
	ButtonStyleRender(self, x, y, w, h, group)

	-- Draw the button string.
	local style, sx, sy = self:GetTextSetup()

	DrawString(self, self:GetString(), style, "center", x + sx, y + sy, w - sx, h - sy)
end

-- PushButton class definition --
class.Define("PushButton", function(PushButton)
	-- Returns: Pushbutton's action
	function PushButton:GetAction()
		return self[_action]
	end
	
	-- Returns: Horizontal text style, offset coordinates
	------------------------------------------------------
	function PushButton:GetTextSetup ()
		return self[_style] or "center", self[_sx] or 0, self[_sy] or 0
	end

	-- action: Action to assign
	----------------------------
	PushButton.SetAction = func_ops.FuncSetter(_action, "Uncallable action", true)
	

	-- style: Horizontal text style to assign
	-- sx, sy: Offset coordinates
	------------------------------------------
	function PushButton:SetTextSetup (style, sx, sy)
		if style == "center" then
			sx = 0
			sy = 0
		end

		self[_style] = style
		self[_sx] = sx
		self[_sy] = sy
	end

	--- Class constructor.
	function PushButton:__cons ()
		SuperCons(self, "Widget")

		-- Signals --
		self:SetMultipleSlots(Signals)
	end
end, { base = "Widget" })