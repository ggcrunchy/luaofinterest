--- Derives from <b><a href="Widget.html">Widget</a></b>.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Standard library imports --
local type = type

-- Imports --
local IsInstance = class.IsInstance
local SuperCons = class.SuperCons

-- Cached methods --
local GetH = class.GetMember("Widget", "GetH")
local GetW = class.GetMember("Widget", "GetW")

-- Image class definition --
class.Define("Image", function(Image)
	-- Gets a dimensional amount
	local function GetDim (I, method, gmethod)
		local picture = I:GetPicture("main")
		local dim = method(I)

		if not dim and picture then
			local graphic = picture:GetGraphic()

			if type(graphic) == "table" or IsInstance(graphic) then
				local method = graphic[gmethod]

				if method then
					dim = method(graphic)
				end
			end
		end

		return dim or 0
	end

	--- Override of @{Widget.Widget:GetH}.
	--
	-- If @{Widget.Widget:SetH} has been called with a non-**nil** value, that value is used.
	--
	-- Otherwise, returns the height of the **"main"** picture, or 0 if absent.
	-- <br><br>TODO: Improve constraints on pictures
	-- @return Height.
	function Image:GetH ()
		return GetDim(self, GetH, "GetHeight")
	end

	--- Override of @{Widget.Widget:GetW}.
	--
	-- If @{Widget.Widget:SetW} has been called with a non-**nil** value, that value is used.
	--
	-- Otherwise, returns the width of the **"main"** picture, or 0 if absent.
	-- <br><br>TODO: Improve constraints on pictures
	-- @return Width.
	function Image:GetW ()
		return GetDim(self, GetW, "GetWidth")
	end

	--- Class constructor.
	function Image:__cons ()
		SuperCons(self, "Widget")
	end
end, { base = "Widget" })