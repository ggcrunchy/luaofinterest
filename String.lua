--- Basic widget convenient for drawing a string. It is not necessary to specify its
-- dimensions when attaching, as these can be deduced from the string itself.
--
-- Derives from <b><a href="Widget.html">Widget</a></b>.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Imports --
local DrawString = widget_ops.DrawString
local SuperCons = class.SuperCons

-- String class definition --
class.Define("String", function(String)
	-- Dimension getters --
	for _, what, func in iterators.ArgsByN(2,
		--- Accessor, override of @{Widget.Widget:GetH}.
		--
		-- The string widget will report its height based on its current string and font.
		-- @function String:GetH
		-- @return Height.
		"GetH", widget_ops.StringGetH,

		--- Accessor.
		-- @function String:GetSize
		-- @return Width.
		-- @return Height.
		"GetSize", widget_ops.StringSize,

		--- Accessor, override of @{Widget.Widget:GetW}.
		--
		-- The string widget will report its width based on its current string and font.
		-- @function String:GetW
		-- @return Width.
		"GetW", widget_ops.StringGetW
	) do
		String[what] = function(S)
			return func(S, S:GetString())
		end
	end

	--- Draws the string in the render rect.
	-- @function Signals:render
	-- @see WidgetGroup.WidgetGroup:Render
	local function Render (S, x, y, w, h)
		DrawString(S, S:GetString(), nil, nil, x, y, w, h)
	end

	--- Class constructor.
	function String:__cons ()
		SuperCons(self, "Widget")

		-- Signals --
		self:SetSlot("render", Render)
	end
end, { base = "Widget" })