--- This class provides an interface for objects that should be able to receive signals
-- and react to them if they have the proper handler.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Standard imports --
local assert = assert
local rawget = rawget

-- Modules --
local table_ops = require("table_ops")
local var_preds = require("var_preds")

-- Imports --
local IsCallable = var_preds.IsCallable
local IsCallableOrNil = var_preds.IsCallableOrNil
local IsTable = var_preds.IsTable

-- Slot store --
local Slots = table_ops.SubTablesOnDemand("k")

-- Signalable class definition --
class.Define("Signalable", function(Signalable)
	-- Returns: Slot, or nil
	local function GetSlot (S, signal)
		local slot_table = rawget(Slots, signal)

		return slot_table and slot_table[S]
	end

	---@function Signalable:GetSlot
	-- @param signal Signal name.
	-- @return Slot, or <b>nil</b> if absent.
	-- @see Signalable:SetSlot
	Signalable.GetSlot = GetSlot

	-- Adds a listener to a signal
	local function Add (S, signal, slot)
		Slots[signal][S] = slot
	end

	--- Multiple-signal variant of <b>Signalable:SetSlot</b>.
	-- @param signals_and_slots Table of (<i>signal</i>, <i>slot</i>) pairs.
	-- @see Signalable:SetSlot
	function Signalable:SetMultipleSlots (signals_and_slots)
		assert(IsTable(signals_and_slots), "Invalid signals / slots table")

		for _, v in pairs(signals_and_slots) do
			assert(IsCallable(v), "Uncallable slot")
		end

		for k, v in pairs(signals_and_slots) do
			Add(self, k, v)
		end
	end

	---@param signal Non-<b>nil</b> signal name.
	-- @param slot The handler to associate with <i>signal</i>, or <b>nil</b> to clear it.
	-- @see Signalable:GetSlot
	function Signalable:SetSlot (signal, slot)
		assert(signal ~= nil, "Invalid signal")
		assert(IsCallableOrNil(slot), "Uncallable slot")

		Add(self, signal, slot)
	end

	--- Sends a signal to this item. If a slot exists, it is called as<br><br>
	-- &nbsp&nbsp&nbsp<i><b>slot(item, ...)</b></i>.
	-- @param signal Non-<b>nil</b> signal name.
	-- @param ... Slot arguments.
	-- @return Call results, if the slot existed. Otherwise, nothing.
	function Signalable:Signal (signal, ...)
		assert(signal ~= nil, "Invalid signal")

		local slot = GetSlot(self, signal)

		if slot then
			return slot(self, ...)
		end
	end
end)