--- Debug utility for dumping formatted strings to an array.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Standard library imports --
local assert = assert
local format = string.format
local type = type

-- Current array --
local Array = {}

--- Appends formatted output strings to an array.
-- @function aprintf
-- @tparam string str Format string.
-- @param ... Format parameters.
aprintf = setmetatable({}, {
	__call = function(_, str, ...)
		Array[#Array + 1] = format(str, ...)
	end,
	__metatable = true
})

---@treturn array Current array used by @{aprintf}.
-- @see aprintf:SetArray
function aprintf:GetArray ()
	return Array
end

--- Sets the current array used by @{aprintf}.
-- @tparam table array Table to assign as current array.
-- @see aprintf:GetArray
function aprintf:SetArray (array)
	assert(type(array) == "table", "SetArray: invalid array")

	Array = array
end