--- A picture provides a common front-end for disparate graphics operations, e.g. texture
-- blits, filled or outline quads, gradient fills, etc., which can be switched out on demand.
--
-- It satisfies the interface for <a href="Widget.html#Widget:SetPicture">widget pictures</a>.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Standard library imports --
local assert = assert

-- Imports --
local NoOp = func_ops.NoOp

-- Unique member keys --
local _graphic = {}
local _props = {}

-- Picture class definition --
class.Define("Picture", function(Picture)
	--- Draws the picture in the given rect, delegating to the current graphic. If none is
	-- assigned, this is a no-op.
	-- @tparam number x Rect x-coordinate.
	-- @tparam number y Rect y-coordinate.
	-- @tparam number w Rect width.
	-- @tparam number h Rect height.
	-- @param props If provided, the set that is passed to the graphic; otherwise, the
	-- picture's property set is used.
	-- @see Picture:SetGraphic, Picture:__cons
	function Picture:Draw (x, y, w, h, props)
		(self[_graphic] or NoOp)(x, y, w, h, props or self[_props])
	end

	---@return Picture graphic, or **nil** if absent.
	-- @see Picture:SetGraphic
	function Picture:GetGraphic ()
		return self[_graphic]
	end

	---@param name Non-**nil** name of property to get.
	-- @return Property value.
	function Picture:GetProperty (name)
		assert(name ~= nil, "name == nil")

		return self[_props][name]
	end

	--- @function Picture:SetGraphic
	-- @param graphic Graphic to assign, or **nil** to remove the graphic.
	--
	-- A valid graphic is callable as
	--    graphic(x, y, w, h, props),
	-- and will draw in the provided rect, customizing according to the options in _props_,
	-- which should be treated as read-only.
	-- @see Picture:__cons
	Picture.SetGraphic = func_ops.FuncSetter(_graphic, "Uncallable graphic", true)

	---@param name Non-**nil** name of property to set.
	-- @param value Value to assign to property.
	function Picture:SetProperty (name, value)
		assert(name ~= nil, "name == nil")

		self[_props][name] = value
	end

	--- Class constructor.
	-- @param graphic Graphic handle.
	-- @param props Reference to a property set, which is a collection of (name, value)
	-- pairs describing how to draw the graphic. If absent, a table is created internally.
	-- @see Picture:GetProperty, Picture:SetGraphic, Picture:SetProperty
	function Picture:__cons (graphic, props)
		self[_props] = props or {}

		self:SetGraphic(graphic)
	end
end)