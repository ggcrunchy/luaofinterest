--- A basic widget that can be clicked on and off, and queried about its state.
--
-- Derives from @{Widget}.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Imports --
local StateSwitch = widget_ops.StateSwitch
local SuperCons = class.SuperCons

-- Unique member keys --
local _is_checked = {}

-- Checkbox class definition --
class.Define("Checkbox", function(Checkbox)
	---@treturn boolean The box is checked?
	function Checkbox:IsChecked ()
		return self[_is_checked] == true
	end

	-- Toggle helper
	local function Toggle (C)
		C[_is_checked] = not C[_is_checked]
	end

	--- Sets the current check state. Toggles will send signals as
	--    signal(self, "toggle"),
	-- where _signal_ will be **switch_from** or **switch_to**.
	-- @param check Check state to assign.
	-- @param always_refresh If true, receive **switch_to** signals even when the
	-- check state does not toggle.
	function Checkbox:SetCheck (check, always_refresh)
		StateSwitch(self, not check ~= not self[_is_checked], always_refresh, Toggle, "toggle")
	end

	-- Stock signals --
	local Signals = {}

	---@function Signals:grab
	-- @see WidgetGroup.WidgetGroup:Execute
	Signals.grab = Toggle

	--- The **"checked"** or **"unchecked"** picture is drawn with the render rect, based
	-- on the current state. The **"frame"** picture is then drawn in the same area.
	-- @function Signals:render
	-- @see WidgetGroup.WidgetGroup:Render
	function Signals:render (x, y, w, h)
		self:DrawPicture(self[_is_checked] and "checked" or "unchecked", x, y, w, h)

		-- Frame the checkbox.
		self:DrawPicture("frame", x, y, w, h)
	end

	--- Class constructor.
	function Checkbox:__cons ()
		SuperCons(self, "Widget")

		-- Signals --
		self:SetMultipleSlots(Signals)
	end
end, { base = "Widget" })