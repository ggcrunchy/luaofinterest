--- This class provides some apparatus for dealing with sequential data, where elements
-- may be inserted and removed often and observors dependent on the positioning must be
-- alerted.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Standard library imports --
local assert = assert
local ipairs = ipairs
local pairs = pairs

-- Imports --
local IsCallable = var_preds.IsCallable
local IsCountable = var_preds.IsCountable
local IndexInRange = numeric_ops.IndexInRange
local New = class.New
local RangeOverlap = numeric_ops.RangeOverlap
local Weak = table_ops.Weak

-- Unique member keys --
local _insert = {}
local _object = {}
local _remove = {}
local _size = {}

-- Sequence state --
local Groups = ...

for _, v in ipairs(Groups) do
	v.elements = table_ops.SubTablesOnDemand()
end

-- Sequence class definition --
class.Define("Sequence", function(Sequence)
	-- Element update helper
	local function UpdateElements (S, op_key, index, count, new_size)
		for _, group in ipairs(Groups) do
			local op = group[op_key]

			for element in pairs(group.elements[S]) do
				op(element, index, count, new_size)
			end
		end
	end

	-- Inserts new items
	-- index: Insertion index
	-- count: Count of items to add
	-- ...: Insertion arguments
	--------------------------------
	function Sequence:Insert (index, count, ...)
		assert(self:IsItemValid(index, true) and count > 0)

		UpdateElements(self, "Insert", index, count, #self + count)

		self[_insert](index, count, ...)
	end

	-- index: Index of item in sequence
	-- is_addable: If true, the end of the sequence is valid
	-- Returns: If true, the item is valid
	---------------------------------------------------------
	function Sequence:IsItemValid (index, is_addable)
		return IndexInRange(index, #self, is_addable)
	end

	-- Returns: Item count
	-----------------------
	function Sequence:__len ()
		local size = self[_size]

		if size then
			return size(self[_object]) or 0
		else
			return #self[_object]
		end
	end

	-- Removes a series of items
	-- index: Removal index
	-- count: Count of items to remove
	-- ...: Removal arguments
	-- Returns: Count of items removed
	-----------------------------------
	function Sequence:Remove (index, count, ...)
		local cur_size = #self

		count = RangeOverlap(index, count, cur_size)

		if count > 0 then
			UpdateElements(self, "Remove", index, count, cur_size - count)

			self[_remove](index, count, ...)
		end

		return count
	end

	--- Class constructor.
	-- @param object Sequenced object.
	-- @param insert Insert routine.
	-- @param remove Remove routine.
	-- @param size Optional size routine.
	function Sequence:__cons (object, insert, remove, size)
		assert(IsCallable(size) or (size == nil and IsCountable(object)), "Invalid sequence parameters")

		-- Sequenced object --
		self[_object] = object

		-- Sequence operations --
		self[_insert] = insert
		self[_remove] = remove
		self[_size] = size
	end
end)