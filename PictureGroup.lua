--- PICTURE_GROUP

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-----------
-- Imports
-----------
local ipairs = ipairs
local format = string.format
local Picture = graphicshelpers.Picture
local Texture = graphicshelpers.Texture

---------------------------------
-- PictureGroup class definition
---------------------------------
class.Define("PictureGroup", {
	-- Caches a picture
	-- name: Texture filename
	-- props: Optional external property set
	-- ...: Texture load arguments
	-- Returns: Picture handle
	-----------------------------------------
	Picture = function(G, name, props, ...)
		return Picture(G:Texture(name, ...), props)	
	end,

	-- Caches a range of pictures
	-- name: Texture filename format string
	-- first, last: Mapping range
	-- aprops: Optional external property set array
	-- ...: Texture load arguments
	-- Returns: Array of picture handles
	------------------------------------------------
	PictureRange = function(G, name, first, last, aprops, ...)
		local group, props = {}

		for i = first, last do
			props = aprops and aprops[#group + 1] or nil

			group[#group + 1] = G:Picture(format(name, i), props, ...)
		end

		return group
	end,

	-- Refreshes cached textures
	-----------------------------
	Refresh = function(G)
		for _, texture in ipairs(G.set) do
			texture:EnsureLoaded()
		end
	end,

	-- Caches a texture
	-- name: Texture filename
	-- ...: Texture load arguments
	-- Returns: Texture handle
	-------------------------------
	Texture = function(G, name, ...)
		local tex = Texture(name, "no_upload", ...)

		G.set[#G.set + 1] = tex

		return tex
	end,

	-- Caches a range of textures
	-- name: Texture filename format string
	-- first, last: Mapping range
	-- ...: Texture load arguments
	-- Returns: Array of texture handles
	----------------------------------------
	TextureRange = function(G, name, first, last, ...)
		local group = {}

		for i = first, last do
			group[#group + 1] = G:Texture(format(name, i), ...)
		end

		return group
	end,
},

-- Constructor
---------------
function(G)
	G.set = {}
end)